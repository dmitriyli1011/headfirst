﻿namespace State
{
    using System;

    public interface IState
    {
        /// <summary>
        /// Вставить монету
        /// </summary>
        void InsertQuarter();

        /// <summary>
        /// Извлечь
        /// </summary>
        void EjectQuarter();

        /// <summary>
        /// Повернуть ручку
        /// </summary>
        void TurnCrank();

        /// <summary>
        /// Выдать 
        /// </summary>
        void Dispense();
    }
}