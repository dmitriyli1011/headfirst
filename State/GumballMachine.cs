﻿namespace State
{
    using System;
    using System.Text;

    using State.States;

    /// <summary>
    /// Машина состояний
    /// </summary>
    public class GumballMachine
    {
        private IState _soldOutState;

        private IState _noQuarterState;

        private IState _hasQuarterState;

        private IState _winnerState;

        private IState _soldState;

        private IState _state;

        private int count = 0;

        public GumballMachine(int numberGumballs)
        {
            _soldOutState = new SoldOutState(this);
            _noQuarterState = new NoQuarterState(this);
            _hasQuarterState = new HasQuarterState(this);
            _soldState = new SoldState(this);
            _winnerState = new WinnerState(this);
            count = numberGumballs;

            if (numberGumballs > 0)
            {
                _state = _noQuarterState;
            }
            else
            {
                _state = _soldOutState;
            }
        }

        public void InsertQuarter()
        {
            _state.InsertQuarter();
        }

        public void EjectQuarter()
        {
            _state.EjectQuarter();
        }

        public void TurnCrank()
        {
            _state.TurnCrank();
            _state.Dispense();
        }

        public void SetState(IState state)
        {
            _state = state;
        }

        public void ReleaseBall()
        {
            Console.WriteLine("A Gumball comes rolling out the slot...");
            if (count != 0)
            {
                count -= 1;
            }
        }

        public int GetCount()
        {
            Console.WriteLine($"Count of ball is {count}");
            return count;
        }

        public IState GetHasQuarterState()
        {
            return _hasQuarterState;
        }

        public IState GetNoQuarterState()
        {
            return _noQuarterState;
        }

        public IState GetSoldOutState()
        {
            return _soldOutState;
        }

        public IState GetSoldState()
        {
            return _soldState;
        }

        public IState GetWinnerState()
        {
            return _winnerState;
        }

        public IState GetCurrentState()
        {
            return _state;
        }

        public override string ToString()
        {
            var info = new StringBuilder();
            info.AppendLine("Inventory " + count + " gumball");
            info.AppendLine("Machine is " + GetCurrentState() + " gumball");
            return info.ToString();
        }
    }
}