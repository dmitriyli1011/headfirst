﻿namespace State.States
{
    using System;

    /// <summary>
    /// Состояние 'победа'
    /// </summary>
    public class WinnerState :IState
    {
        private GumballMachine _gumballMachine;
        public WinnerState(GumballMachine gumballMachine)
        {
            _gumballMachine = gumballMachine;
        }
        public void InsertQuarter()
        {
            Console.WriteLine("Please wait, we're already giving you a Gumball");
        }

        public void EjectQuarter()
        {
            Console.WriteLine("Please wait, we're already giving you a Gumball");
        }

        public void TurnCrank()
        {
            Console.WriteLine("Please wait, we're already giving you a Gumball");
        }

        public void Dispense()
        {
            _gumballMachine.ReleaseBall();
            if (_gumballMachine.GetCount() == 0)
            {
                _gumballMachine.SetState(_gumballMachine.GetSoldOutState());
            }
            else
            {
                _gumballMachine.ReleaseBall();
                Console.WriteLine("YOU'RE A WINNER! You got two gumballs for your quarter");

                if (_gumballMachine.GetCount() > 0)
                {
                    _gumballMachine.SetState(_gumballMachine.GetNoQuarterState());
                }
                else
                {
                    Console.WriteLine("Oops, out of gumballs!");
                }
            }
        }

        public override string ToString()
        {
            return "Winner State";
        }
    }
}