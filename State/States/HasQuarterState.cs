﻿namespace State.States
{
    using System;

    /// <summary>
    /// Состояние 'Положена монетка'
    /// </summary>
    public class HasQuarterState : IState
    {
        private GumballMachine _gumballMachine;

        /// <summary>
        /// Распространенной идиомой в Java является использование currentTimeMillis() для целей
        /// синхронизации или планирования, когда вас не интересуют фактические миллисекунды с 1970 года,
        /// а вместо этого вычисляют некоторое относительное значение и
        /// сравнивают последующие вызовы currentTimeMillis() с этим значением.
        /// Если это то, что вы ищете, то эквивалент C# равен Environment.TickCount .
        /// </summary>
        private Random randomWinner = new Random(Millis);

        private static readonly DateTime Jan1St1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary> Get extra long current timestamp </summary>
        public static int Millis
        {
            get
            {
                return (int)((DateTime.UtcNow - Jan1St1970).TotalMilliseconds);
            }
        }

        public HasQuarterState(GumballMachine gumballMachine)
        {
            _gumballMachine = gumballMachine;
        }
        public void InsertQuarter()
        {
            Console.WriteLine("You can't insert another quarter");
        }

        public void EjectQuarter()
        {
            Console.WriteLine("Quarter returned");
            _gumballMachine.SetState(_gumballMachine.GetNoQuarterState());
        }

        public void TurnCrank()
        {
            Console.WriteLine("You turned...");
            var winner = randomWinner.Next(10);
            if (winner == 0 && _gumballMachine.GetCount() > 1)
            {
                _gumballMachine.SetState(_gumballMachine.GetWinnerState());
            }
            else
            {
                _gumballMachine.SetState(_gumballMachine.GetSoldState());
            }
        }

        public void Dispense()
        {
            Console.WriteLine("No gumball dispensed");
        }

        public override string ToString()
        {
            return "Has Quarter State";
        }
    }
}