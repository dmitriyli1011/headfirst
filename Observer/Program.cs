﻿using System;

namespace Observer
{
    using global::Observer.Observers;
    using global::Observer.Subjects;

    class Program
    {
        /// <summary>
        /// Паттерн поведения.
        /// Наблюдатель определяет отношение один ко многим.
        /// При изменении одного, связанные с ним оповещяются и изменяются.
        /// (Все благодаря ссылкам (^-^) )
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            WeatherData weatherData = new WeatherData();

            CurrentConditionsDisplay conditionsDisplay = new CurrentConditionsDisplay(weatherData);
            ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);
            StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
            HeatIndexDisplay heatIndexDisplay = new HeatIndexDisplay(weatherData);

            weatherData.SetMesurements(80, 65, 30.4f);
            Console.WriteLine("-----------------next-----------------");
            weatherData.SetMesurements(82, 70, 29.2f);
            Console.WriteLine("-----------------next-----------------");
            weatherData.SetMesurements(78, 90, 29.2f);
            Console.WriteLine("-----------------next-----------------");
        }
    }
}
