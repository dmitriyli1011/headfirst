﻿namespace Observer.Observers
{
    /// <summary>
    /// Подписчик должен реализовать.
    /// </summary>
    public interface IObserver
    {
        /// <summary>
        /// Обновление информации о состоянии.
        /// <param name="temp">Температура. </param>
        /// <param name="humidity"> Влажность. </param>
        /// <param name="pressure"> Давление. </param>
        /// </summary>
        void Update(float temp, float humidity, float pressure);
    }
}