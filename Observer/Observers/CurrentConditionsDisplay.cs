﻿namespace Observer.Observers
{
    using System;

    using Observer.Actions;
    using Observer.Subjects;

    /// <summary>
    /// Наблюдатель текущее состояние.
    /// </summary>
    public class CurrentConditionsDisplay :
        IObserver,
        IDisplayElement
    {
        /// <summary>
        /// Интерфейс Издателя/субъекта
        /// </summary>
        private readonly ISubject _weatherData;

        /// <summary>
        /// Влажность.
        /// </summary>
        public float Humidity;

        /// <summary>
        /// Температура.
        /// </summary>
        public float Temperature;

        public CurrentConditionsDisplay(ISubject weatherData)
        {
            _weatherData = weatherData;
            _weatherData.RegisterObserver(this);
        }

        public void Display()
        {
            Console.WriteLine("CurrentConditionsDisplay\n" +
                              $"Current conditional temperature {Temperature} C and humidity {Humidity}\n");
        }

        public void Update(float temp, float humidity, float pressure)
        {
            Humidity = humidity;
            Temperature = temp;
            Display();
        }
    }
}