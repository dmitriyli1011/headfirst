﻿namespace Observer.Observers
{
    using System;

    using Observer.Actions;
    using Observer.Subjects;

    /// <summary>
    /// Наблюдатель статистки.
    /// </summary>
    public class StatisticsDisplay :
        IObserver,
        IDisplayElement

    {
        private float maxTemp = 0.0f;
        private float minTemp = 200;
        private float tempSum = 0.0f;
        private int numReadings;

        private ISubject _subject;
        public StatisticsDisplay(ISubject subject)
        {
            _subject = subject;
            _subject.RegisterObserver(this);
        }

        public void Update(float temp, float humidity, float pressure)
        {
            tempSum += temp;
            numReadings++;
            if (temp > maxTemp)
            {
                maxTemp = temp;
            }

            if (temp < minTemp)
            {
                minTemp = temp;
            }

            Display();
        }

        public void Display()
        {
            Console.WriteLine("StatisticsDisplay\nAvg/Max/Min temperature = " + (tempSum / numReadings)
                                                           + "/" + maxTemp + "/" + minTemp+"\n");
        }
    }
}