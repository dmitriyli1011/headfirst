﻿namespace Observer.Observers
{
    using System;

    using Observer.Actions;
    using Observer.Subjects;

    /// <summary>
    /// Наблюдатель прогноза
    /// </summary>
    public class ForecastDisplay :
        IObserver,
        IDisplayElement
    {
        /// <summary>
        /// Интерфейс Издателя/субъекта
        /// </summary>
        private readonly ISubject _weatherData;

        /// <summary>
        /// ТЕкущее давление.
        /// </summary>
        private float currentPressure = 29.92f;

        /// <summary>
        /// Проштое давление
        /// </summary>
        private float lastPressure;

        public ForecastDisplay(ISubject weatherData)
        {
            _weatherData = weatherData;
            _weatherData.RegisterObserver(this);
        }

        public void Display()
        {
            Console.WriteLine("Forecast: ");
            if (currentPressure > lastPressure)
                Console.WriteLine("Improving weather on the way!\n");
            else if (currentPressure == lastPressure)
                Console.WriteLine("More of the same\n");
            else if (currentPressure < lastPressure)
                Console.WriteLine("Watch out for cooler, rainy weather\n");
        }

        public void Update(float temp, float humidity, float pressure)
        {
            lastPressure = currentPressure;
            currentPressure = pressure;

            Display();
        }
    }
}