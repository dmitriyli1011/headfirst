﻿namespace Observer.Subjects
{
    using System.Collections.Generic;

    using Observer.Observers;

    /// <summary>
    /// Субъект / Издатель
    /// </summary>
    public class WeatherData :
        ISubject
    {
        /// <summary>
        /// Контейнер подписчиков.
        /// </summary>
        private readonly List<IObserver> _observers;

        /// <summary>
        /// Влажность.
        /// </summary>
        public float Humidity;

        /// <summary>
        /// Давление.
        /// </summary>
        public float Pressure;

        /// <summary>
        /// Температура.
        /// </summary>
        public float Temperature;

        public WeatherData()
        {
            _observers = new List<IObserver>();
        }


        public void NotifyObserver()
        {
            foreach (var observer in _observers)
            {
                observer.Update(Temperature, Humidity, Pressure);
            }
        }

        public void RegisterObserver(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void SetMesurements(float temp, float humidity, float pressure)
        {
            Temperature = temp;
            Humidity = humidity;
            Pressure = pressure;
            //Вызывает Update подписчика,
            //изменяя состояние обекта по переданной ссылке
            MeasurementChanged();
        }

        /// <summary>
        /// Вызывает обновление данных у подписчиков,
        /// так как их ссылки содержаться в списке
        /// данные обновлятся связанно у тех кто содержится списке.
        /// </summary>
        private void MeasurementChanged()
        {
            NotifyObserver();
        }
    }
}