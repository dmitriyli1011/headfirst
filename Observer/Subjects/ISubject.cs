﻿namespace Observer.Subjects
{
    using Observer.Observers;

    /// <summary>
    /// Интерфейс для субъекта Publisher.
    /// </summary>
    public interface ISubject
    {
        /// <summary>
        /// Уведомить.
        /// </summary>
        void NotifyObserver();

        /// <summary>
        /// Регистрация подписчика.
        /// </summary>
        void RegisterObserver(IObserver observer);

        /// <summary>
        /// Снятие с подписки.
        /// </summary>
        void RemoveObserver(IObserver observer);
    }
}