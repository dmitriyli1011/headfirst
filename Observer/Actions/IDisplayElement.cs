﻿namespace Observer.Actions
{
    /// <summary>
    /// Для реализации отображения.
    /// </summary>
    public interface IDisplayElement
    {
        void Display();
    }
}