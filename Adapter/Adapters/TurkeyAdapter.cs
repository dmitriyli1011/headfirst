﻿namespace Adapter.Adapters
{
    using Adapter.Interfaces;

    /// <summary>
    /// Адаптер реализует целевой
    /// интерфейс и хранит ссылку
    /// на экземпляр адаптируемого
    /// объекта.
    /// </summary>
    public class TurkeyAdapter : IDuck
    {
        private ITurkey _turkey;

        public TurkeyAdapter(ITurkey turkey)
        {
            _turkey = turkey;
        }
        public void Quack()
        {
            _turkey.Gobble();
        }

        public void Fly()
        {
            for (var i = 0; i < 5; i++)
            {
                _turkey.Fly();
            }
        }
    }
}