﻿namespace Adapter
{
    using System;

    using Adapter.Adapters;
    using Adapter.Ducks;
    using Adapter.Interfaces;
    using Adapter.Turkeys;

    internal class Program
    {
        /// <summary>
        /// Структурный паттерн
        ///
        /// Предназначен для преобразования интерфейса одного класса в интерфейс другого.
        /// Благодаря реализации данного паттерна мы можем использовать вместе классы с несовместимыми интерфейсами.
        /// 
        /// Адаптер реализует целевой интерфейс и хранит ссылку
        /// на экземпляр адаптируемого объекта.
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            MallarDuck mallarDuck = new MallarDuck();
            TestDuck(mallarDuck);
            Console.WriteLine("---");
            WildTurkey wildTurkey = new WildTurkey();
            TurkeyAdapter turkeyAdapter = new TurkeyAdapter(wildTurkey);
            TestDuck(turkeyAdapter);

        }

        private static void TestDuck(IDuck duck)
        {
            duck.Fly();
            duck.Quack();
        }
    }
}