﻿namespace Adapter.Interfaces
{
    public interface ITurkey
    {
        public void Fly();
        public void Gobble();
    }
}