﻿namespace Adapter.Ducks
{
    using System;

    using Adapter.Interfaces;

    public class MallarDuck : IDuck
    {
        public void Quack()
        {
            Console.WriteLine("Quack");
        }

        public void Fly()
        {
            Console.WriteLine("I'm flying");
        }
    }
}