﻿using System;

namespace SimpleFactory
{
    /// <summary>
    /// Порождающий паттерн.
    ///  (с.149)
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            PizzaStore pizzaStore = new PizzaStore();
            pizzaStore.OrderPizza("cheese");

            Console.WriteLine();
            pizzaStore.OrderPizza("peperoni");
        }
    }
}
