﻿namespace SimpleFactory
{
    using System;

    public class PizzaStore
    {
        protected SimplePizzaFactory factory;

        public PizzaStore()
        {
            factory = new SimplePizzaFactory();
        }

        public Pizza OrderPizza(string type)
        {
            var pizza = factory.CreatePizza(type);
            if (pizza is null)
                throw new ArgumentException("Pizza type not found");

            Console.WriteLine(pizza.Description);
            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();
            return pizza;
        }
    }
}