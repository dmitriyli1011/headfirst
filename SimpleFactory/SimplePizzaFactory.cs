﻿namespace SimpleFactory
{
    /// <summary>
    /// Простоя фабрика инкапсулирует подробности создания объектов.
    /// </summary>
    public class SimplePizzaFactory
    {
        public Pizza CreatePizza(string type)
        {
            return type switch
            {
                "peperoni" => new PeperoniPizza(),
                "clam" => new ClaimPizza(),
                "veggie" => new VeggiePizza(),
                "cheese" => new CheesePizza(),
                _ => null
            };
        }
    }
}