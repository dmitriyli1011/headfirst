﻿using System;

namespace SimpleFactory
{
    using Factory.Factories;

    using FactoryMethod.Factories;

    /// <summary>
    ///  (с.163, с.166)
    /// Порождающий паттерн.
    /// Фабричный метод определяет интерфейс создания
    /// объекта, но позволяет субклассам выбрать класс создаваемо-
    /// го экземпляра.Таким образом, Фабричный Метод делегирует
    /// операцию создания экземпляра субклассам..
    ///
    /// класс-создатель не обладает информацией о фактическом типе создавае-
    /// мых продуктов
    /// Инкапсулирует создание объектов.
    ///
    /// Слабая связь: один из примеров - работа с абстрактным_классом/интерфейсом
    /// так как они не являются конкретными классами и их объект создать невозможно.
    /// Логика строится на абстрактным_классом/интерфейсом,
    /// материализация применется в местах получения конкретного объекта,
    /// который реализует интерфейс или наследует абстрактный класс. (с.154)
    ///
    /// один конкретный создатель — он
    /// отделяет реализацию продукта от его использования
    /// 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            PizzaStore pizzaStore = new ChicagoPizzaStore();
           var nypizza=  pizzaStore.OrderPizza("cheese");

            Console.WriteLine();

            pizzaStore.OrderPizza("pepperoni");
        }
    }
}
