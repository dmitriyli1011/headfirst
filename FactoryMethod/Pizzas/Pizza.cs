﻿namespace FactoryMethod.Pizzas
{
    using System;

    public abstract class Pizza
    {
        public string Description { get; set; }

        public void Prepare() { Console.WriteLine("Prepare"); }
        public void Bake() { Console.WriteLine("Bake for 25 minutes at 350"); }
        public void Cut() { Console.WriteLine("Cutting the pizza into diagonal slices"); }
        public void Box() { Console.WriteLine("Place pizza in official PizzaStore box"); }
    }

    #region SimplePizza
    public class PeperoniPizza : Pizza
    {
        public PeperoniPizza()
        {
            Description = "Peperoni Pizza";
        }
    }
    public class ClaimPizza : Pizza
    {
        public ClaimPizza()
        {
            Description = "Claim Pizza";
        }
    }
    public class VeggiePizza : Pizza
    {
        public VeggiePizza()
        {
            Description = "Viggie Pizza";
        }
    }

    public class CheesePizza : Pizza
    {
        public CheesePizza()
        {
            Description = "Cheese Pizza";
        }
    }

    #endregion

    #region ChicagoPizza

    public class ChicagoStyleCheesePizza : Pizza
    {
        public ChicagoStyleCheesePizza()
        {
            Description = "Chicago Style Cheese Pizza";
        }
    }
    public class ChicagoStylePepperoniPizza : Pizza
    {
        public ChicagoStylePepperoniPizza()
        {
            Description = "Chicago Style Pepperoni Pizza";
        }
    }
    public class ChicagoStyleClamPizza : Pizza
    {
        public ChicagoStyleClamPizza()
        {
            Description = "Chicago Style Clam Pizza";
        }
    }
    public class ChicagoStyleVeggiePizza : Pizza
    {
        public ChicagoStyleVeggiePizza()
        {
            Description = "Chicago Style Veggie Pizza";
        }
    }

    #endregion

    #region NYPizza

    public class NYStyleCheesePizza : Pizza
    {
        public NYStyleCheesePizza()
        {
            Description = "NYStyle Cheese Pizza";
        }
    }
    public class NYStylePepperoniPizza : Pizza
    {
        public NYStylePepperoniPizza()
        {
            Description = "NYStyle Pepperoni Pizza";
        }
    }
    public class NYStyleClamPizza : Pizza
    {
        public NYStyleClamPizza()
        {
            Description = "NYStyle Clam Pizza";
        }
    }
    public class NYStyleVeggiePizza : Pizza
    {
        public NYStyleVeggiePizza()
        {
            Description = "NYStyle Veggie Pizza";
        }
    }

    #endregion

}