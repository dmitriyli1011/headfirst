﻿namespace FactoryMethod.Factories
{
    using FactoryMethod.Pizzas;

    public class NYPizzaStore : PizzaStore
    {
        protected override Pizza CreatePizza(string type)
        {
            return type switch
            {
                "cheese" => new NYStyleCheesePizza(),
                "pepperoni" => new NYStylePepperoniPizza(),
                "claim" => new NYStyleClamPizza(),
                "veggie" => new NYStyleVeggiePizza(),
                _ => null
            };
        }
    }
}