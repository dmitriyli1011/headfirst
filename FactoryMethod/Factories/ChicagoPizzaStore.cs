﻿namespace Factory.Factories
{
    using FactoryMethod.Factories;
    using FactoryMethod.Pizzas;

    public class ChicagoPizzaStore : PizzaStore
    {
        protected override Pizza CreatePizza(string type)
        {
            return type switch
            {
                "cheese" => new ChicagoStyleCheesePizza(),
                "pepperoni" => new ChicagoStylePepperoniPizza(),
                "claim" => new ChicagoStyleClamPizza(),
                "veggie" => new ChicagoStyleVeggiePizza(),
                _ => null
            };
        }
    }
}