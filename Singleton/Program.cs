﻿using System;

namespace Singleton
{
    using System.Threading;

    /// <summary>
    /// Порождающий паттерн.
    ///
    /// 
    /// 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Thread thread1 = new Thread(() =>
                GetSingletonFirst("first"));

            Thread thread2 = new Thread(() =>
                GetSingletonFirst("second"));

            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();

        }

        public static void GetSingletonFirst(string value)
        {
            SingleObject instance = SingleObject.GetInstance(value);
            Console.WriteLine(instance.GetDescription());
            Console.WriteLine(instance.GetHashCode());
        }
    }
}
