﻿namespace Singleton
{
    public class SingleObject
    {
        private static readonly object _lock = new object();
        private static SingleObject _instance;
        private readonly string _value;

        private SingleObject(string description)
        {
            _value = description;

        }

        /// <summary>
        /// "блокировка с двойной проверкой"
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static SingleObject GetInstance(string description)
        {
            if (_instance is null)
            {
                // Здесь может уже создасться экземпляр, поэтому в lock
                // еще проверка.
                lock (_lock)
                {
                    // Проверяется второй раз так как между первой проверкой
                    // и блокированием объекта, в параллельном
                    // потоке мог создасться экземпляр.
                    if(_instance is null) 
                        _instance = new SingleObject(description);
                }
            }

            return _instance;
        }
        public string GetDescription()
        {
            return _value;
        }
    }

    public class SecondSingleObject
    {
        private static object _lock = new object();
        private static volatile SecondSingleObject _instance;
        private readonly string _description;
        private SecondSingleObject()
        {
            _description = "Object was created";
        }

        public static SecondSingleObject GetInstance()
        {
            if (_instance is null)
            {
                lock(_lock)
                {
                    _instance ??= new SecondSingleObject();
                }
            }

            return _instance;
        }

        public string GetDescription()
        {
            return _description;
        }
    }
}