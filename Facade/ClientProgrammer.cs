﻿namespace Facade
{
    using Facade.Facades;

    public class ClientProgrammer
    {
        public void CreateApplication(VsFacade vsFacade)
        {
            vsFacade.Start();
            vsFacade.Stop();
        }
    }
}