﻿namespace Facade
{
    using System;

    using Facade.Components;
    using Facade.Facades;

    /// <summary>
    /// Фассад это структурный паттерн проектирования,
    ///  который предоставляет простой интерфейс к сложной системе классов, библиотеке или фреймворку.
    ///
    /// Фасад не только упрощает интерфейс, но и обеспечивает логическую изоляцию
    /// клиента от подсистемы, состоящей  из многих компонентов.
    ///
    /// Фасад применяется для упрощения,
    /// а адаптер — для преобразования интерфейса
    /// к другой форме.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            var clr = new CLR();
            var textEditor = new TextEditor();
            var compiler = new Compiler();

            var facade = new VsFacade(clr, compiler, textEditor);

            var client = new ClientProgrammer();
            client.CreateApplication(facade);
        }
    }
}