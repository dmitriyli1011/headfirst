﻿namespace Facade.Facades
{
    using Facade.Components;

    public class VsFacade
    {
        private CLR _clr;
        private Compiler _compiler;
        private TextEditor _textEditor;

        public VsFacade(CLR clr, Compiler compiler, TextEditor textEditor)
        {
            _clr = clr;
            _compiler = compiler;
            _textEditor = textEditor;
        }

        public void Start()
        {
            _textEditor.CreateCode();
            _textEditor.Save();
            _compiler.Compile();
            _clr.Execute();
        }

        public void Stop()
        {
            _clr.Finish();
        }
    }
}