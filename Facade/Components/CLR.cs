﻿namespace Facade.Components
{
    using System;

    public class CLR
    {
        public void Execute()
        {
            Console.WriteLine("Выполнение приложения");
        }

        public void Finish()
        {
            Console.WriteLine("Завершение работы приложения");
        }
    }
}