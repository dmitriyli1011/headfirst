﻿namespace Decorator.Models.Base
{
    using System.Text;

    /// <summary>
    /// Напиток базовый объект.
    /// </summary>
    public abstract class Beverage
    {
        /// <summary>
        /// Описание
        /// </summary>
        public StringBuilder Description = new StringBuilder("Unknown Beverage.\n");

        /// <summary>
        /// Описание.
        /// </summary>
        /// <returns></returns>
        public virtual string GetDescription()
        {
            return Description.ToString();
        }

        /// <summary>
        /// Вычисление цены.
        /// </summary>
        /// <returns> Расчет цены. </returns>
        public abstract double Coast();
    }
}