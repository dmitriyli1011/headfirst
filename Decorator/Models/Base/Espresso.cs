﻿namespace Decorator.Models.Base
{
    using System.Text;

    public class Espresso : Beverage
    {
        public Espresso()
        {
            Description = new StringBuilder("Espresso.\n");
        }

        public override double Coast()
        {
            return 1.99;
        }
    }
}