﻿namespace Decorator.Models.Base
{
    using System.Text;

    public class DarkRoast : Beverage
    {
        public DarkRoast()
        {
            Description = new StringBuilder("Dark Roast Coffee\n");
        }
        public override double Coast()
        {
            return 2.0;
        }
    }
}