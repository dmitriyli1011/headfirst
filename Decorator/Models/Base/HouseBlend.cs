﻿namespace Decorator.Models.Base
{
    using System.Text;

    public class HouseBlend : Beverage
    {
        public HouseBlend()
        {
            Description = new StringBuilder("House Blend Coffee.\n");
        }

        public override double Coast()
        {
            return .89;
        }
    }
}