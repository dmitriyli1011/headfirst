﻿namespace Decorator.Models.Additions
{
    using Decorator.Models.Base;

    public class Whip : CondimentDecorator
    {
        private Beverage _beverage;

        public Whip(Beverage beverage)
        {
            _beverage = beverage;
        }
        public override string GetDescription()
        {
            return _beverage.GetDescription() + " Whip.\n";
        }

        public override double Coast()
        {
            return 0.8 + _beverage.Coast();
        }
    }
}