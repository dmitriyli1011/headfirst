﻿namespace Decorator.Models.Additions
{
    using Decorator.Models.Base;

    public class Soy : CondimentDecorator
    {
        private Beverage _beverage;

        public Soy(Beverage beverage)
        {
            _beverage = beverage;
        }
        public override string GetDescription()
        {
            return _beverage.GetDescription() + " Soy.";
        }

        public override double Coast()
        {
            return 0.25 + _beverage.Coast();
        }
    }
}