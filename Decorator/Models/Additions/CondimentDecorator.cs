﻿namespace Decorator.Models.Additions
{
    using Decorator.Models.Base;

    /// <summary>
    /// Базовый для дополнений / декораторов.
    /// </summary>
    public abstract class CondimentDecorator : Beverage
    {
    }
}