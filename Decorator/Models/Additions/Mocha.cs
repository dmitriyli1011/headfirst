﻿namespace Decorator.Models.Additions
{
    using Decorator.Models.Base;

    public class Mocha : CondimentDecorator
    {
        private readonly Beverage _beverage;

        public Mocha(Beverage beverage)
        {
            _beverage = beverage;
        }
        public override string GetDescription()
        {
            return _beverage.GetDescription() + " Mocha.";
        }

        public override double Coast()
        {
            return 0.2 + _beverage.Coast();
        }
    }
}