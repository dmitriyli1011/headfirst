﻿using System;

namespace Decorator
{
    using Decorator.Models.Additions;
    using Decorator.Models.Base;

    /// <summary>
    /// Декоратор использует наследование, чтобы определить супертип
    /// который используется в конструкторе конкретного декоратора.
    ///
    /// Структурный паттерн. Динамически наделяет созданный объект дополнительным
    /// функционалом. Является альтернативой чистому наследованию в области расширения функционала
    /// класса / объекта.  
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Beverage beverage = new DarkRoast();
            Console.WriteLine(beverage.GetDescription() + "$" + beverage.Coast() + "\n----------");

            Beverage beverage1 = new Espresso();
            beverage1 = new Mocha(beverage1);
            beverage1 = new Mocha(beverage1);
            beverage1 = new Whip(beverage1);

            Console.WriteLine(beverage1.GetDescription() + "$" + beverage1.Coast() + "\n----------");

            Beverage beverage2 = new HouseBlend();
            beverage2 = new Soy(beverage2);
            beverage2 = new Mocha(beverage2);
            beverage2 = new Whip(beverage2);

            Console.WriteLine(beverage2.GetDescription() + "$" + beverage2.Coast() + "\n----------");

        }
    }
}
