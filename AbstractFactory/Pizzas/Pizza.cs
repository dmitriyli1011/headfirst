﻿namespace AbstractFactory.Pizzas
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AbstractFactory.Ingredient;

    public abstract class Pizza
    {
        public string Name { get; set; }
        protected Dough _dough;
        protected Sauce _sauce;
        protected List<Vegetable> _vegetables;
        protected Cheese _cheese;
        protected Pepperoni _pepperoni;
        protected Clams _clams;

        public string Description { get; set; }

        public abstract void Prepare();
        public void Bake() { Console.WriteLine("Bake for 25 minutes at 350"); }
        public void Cut() { Console.WriteLine("Cutting the pizza into diagonal slices"); }
        public void Box() { Console.WriteLine("Place pizza in official PizzaStore box"); }

        public string GetPizzaName()
        {
            return Name;
        }
        public void SetPizzaName(string name)
        {
             Name = name;
        }
    }

    public class CheesePizza : Pizza
    {
        private readonly IPizzaIngredientFactory _pizzaIngredientFactory;

        public CheesePizza(IPizzaIngredientFactory pizzaIngredientFactory)
        {
            _pizzaIngredientFactory = pizzaIngredientFactory;
        }

        public override void Prepare()
        {
            Console.WriteLine("Preparing" + Name);
            _dough = _pizzaIngredientFactory.CreateDough();
            _sauce = _pizzaIngredientFactory.CreateSauce();
            _cheese = _pizzaIngredientFactory.CreateCheese();
        }
    }

    public class ClaimPizza : Pizza
    {
        private readonly IPizzaIngredientFactory _pizzaIngredientFactory;

        public ClaimPizza(IPizzaIngredientFactory pizzaIngredientFactory)
        {
            _pizzaIngredientFactory = pizzaIngredientFactory;
        }

        public override void Prepare()
        {
            Console.WriteLine("Preparing" + Name);
            _dough = _pizzaIngredientFactory.CreateDough();
            _sauce = _pizzaIngredientFactory.CreateSauce();
            _cheese = _pizzaIngredientFactory.CreateCheese();
            _clams = _pizzaIngredientFactory.CreateClam();
        }
    }

    public class PepperoniPizza : Pizza
    {
        private readonly IPizzaIngredientFactory _pizzaIngredientFactory;

        public PepperoniPizza(IPizzaIngredientFactory pizzaIngredientFactory)
        {
            _pizzaIngredientFactory = pizzaIngredientFactory;
        }

        public override void Prepare()
        {
            Console.WriteLine("Preparing" + Name);
            _dough = _pizzaIngredientFactory.CreateDough();
            _sauce = _pizzaIngredientFactory.CreateSauce();
            _cheese = _pizzaIngredientFactory.CreateCheese();
            _pepperoni = _pizzaIngredientFactory.CreatePepperoni();
        }
    }

    public class VeggiePizza : Pizza
    {
        private readonly IPizzaIngredientFactory _pizzaIngredientFactory;

        public VeggiePizza(IPizzaIngredientFactory pizzaIngredientFactory)
        {
            _pizzaIngredientFactory = pizzaIngredientFactory;
        }

        public override void Prepare()
        {
            Console.WriteLine("Preparing" + Name);
            _dough = _pizzaIngredientFactory.CreateDough();
            _sauce = _pizzaIngredientFactory.CreateSauce();
            _cheese = _pizzaIngredientFactory.CreateCheese();
            _vegetables = _pizzaIngredientFactory.CreateVeggies().ToList();
        }
    }
}
