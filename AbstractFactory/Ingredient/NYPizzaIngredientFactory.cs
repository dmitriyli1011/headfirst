﻿namespace AbstractFactory.Ingredient
{
    using System.Collections.Generic;

    public class NYPizzaIngredientFactory : IPizzaIngredientFactory
    {
        public Dough CreateDough()
        {
            return new ThinCrustDough();
        }

        public Sauce CreateSauce()
        {
            return new MarinaraSauce();
        }

        public Cheese CreateCheese()
        {
            return new ReggianoCheese();
        }

        public IList<Vegetable> CreateVeggies()
        {
            return new List<Vegetable>()
            {
                new Garlic(),
                new Mushroom(),
                new RedPepper()
            };
        }

        public Pepperoni CreatePepperoni()
        {
            return new SlicedPepperoni();
        }

        public Clams CreateClam()
        {
            return new FreshClams();
        }
    }
}