﻿namespace AbstractFactory.Ingredient
{
    using System.Collections.Generic;

    /// <summary>
    /// Абстрактная фабрика это!
    ///
    /// Семейство продуктов - каждый продукт не
    /// наследуется от одного базового, но
    /// логически связан, применяются в одном месте.
    ///
    /// Причем фабричные метод Create...() возвращает
    ///  абстрактный тип.
    ///
    ///  (с.193)
    /// 
    /// </summary>
    public interface IPizzaIngredientFactory
    {
        public Dough CreateDough();
        public Sauce CreateSauce();
        public Cheese CreateCheese();
        public IList<Vegetable> CreateVeggies();
        public Pepperoni CreatePepperoni();
        public Clams CreateClam();
    }
}