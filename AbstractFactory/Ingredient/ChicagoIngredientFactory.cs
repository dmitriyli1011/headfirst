﻿namespace AbstractFactory.Ingredient
{
    using System.Collections.Generic;

    public class ChicagoIngredientFactory : IPizzaIngredientFactory
    {
        public Dough CreateDough()
        {
            return new ThickCrustDough();
        }

        public Sauce CreateSauce()
        {
            return new PlumTomatoSauce();
        }

        public Cheese CreateCheese()
        {
            return new MozzarellaCheese();
        }

        public IList<Vegetable> CreateVeggies()
        {
            return new List<Vegetable>()
            {
                new EggPlant(),
                new Spinach(),
                new BlackOlives()
            };
        }

        public Pepperoni CreatePepperoni()
        {
            return new SlicedPepperoni();
        }

        public Clams CreateClam()
        {
            return new FrozenClams();
        }
    }
}