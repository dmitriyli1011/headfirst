﻿namespace AbstractFactory.Ingredient
{
   public class Ingredient
    { }

    #region Dough
    public abstract class Dough
    { }

    public class ThinCrustDough : Dough
    { }

    public class ThickCrustDough : Dough
    { }
    #endregion

    #region Sauses
    public abstract class Sauce
    { }
    public class MarinaraSauce : Sauce
    { }

    public class PlumTomatoSauce : Sauce
    { }
    #endregion

    #region Cheeses
    public abstract class Cheese
    { }

    public class ReggianoCheese : Cheese
    { }

    public class MozzarellaCheese : Cheese
    { }
    #endregion

    #region Vegetables
    public abstract class Vegetable
    { }

    public class Garlic : Vegetable
    { }
    public class Mushroom : Vegetable
    { }
    public class RedPepper : Vegetable
    { }
    public class Spinach : Vegetable
    { }

    public class BlackOlives : Vegetable
    { }

    public class EggPlant : Vegetable
    { }
    #endregion

    #region Pepperonies
    public abstract class Pepperoni
    { }

    public class SlicedPepperoni : Pepperoni
    { }

    #endregion

    #region Clamses
    public abstract class Clams
    { }

    public class FreshClams : Clams
    { }

    public class FrozenClams : Clams
    { }
    #endregion

}