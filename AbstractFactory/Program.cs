﻿namespace AbstractFactory
{
    using System;

    using AbstractFactory.Factories;
    using AbstractFactory.Ingredient;

    class Program
    {
        /// <summary>
        /// Порождающий паттерн.
        /// Абстрактная Фабрика предоставляет интерфейс
        /// создания семейств взаимосвязанных или взаимозависимых
        /// объектов без указания их конкретных классов.
        ///
        /// (с.189, с.191)
        /// <see cref="IPizzaIngredientFactory"/>
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            PizzaStore pizzaStore = new NYPizzaStore();
            pizzaStore.OrderPizza("cheese");

            Console.WriteLine();
            PizzaStore pizzaStore2 = new ChicagoPizzaStore();
            pizzaStore2.OrderPizza("pepperoni");
        }
    }
}
