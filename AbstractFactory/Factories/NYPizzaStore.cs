﻿namespace AbstractFactory.Factories
{
    using AbstractFactory.Ingredient;
    using AbstractFactory.Pizzas;

    public class NYPizzaStore : PizzaStore
    {
        
        protected override Pizza CreatePizza(string type)
        {
            var factory = new NYPizzaIngredientFactory();
            Pizza pizza;
            switch (type)
            {
                case "cheese":
                    pizza = new CheesePizza(factory);
                    pizza.SetPizzaName("New York Style Cheese Pizza");
                    return pizza;
                case "pepperoni":
                    pizza = new PepperoniPizza(factory);
                    pizza.SetPizzaName("New York Style Pepperoni Pizza");
                    return pizza;
                case "claim":
                    pizza = new ClaimPizza(factory);
                    pizza.SetPizzaName("New York Style Claim Pizza");
                    return  pizza;
                case "veggie":
                    pizza = new VeggiePizza(factory);
                    pizza.SetPizzaName("New York Style Veggie Pizza");
                    return pizza;
                default:
                    return null;
            }
        }
    }
}