﻿namespace AbstractFactory.Factories
{
    using System;

    using AbstractFactory.Pizzas;

    public abstract class PizzaStore
    {
        
        public Pizza OrderPizza(string type)
        {
            var pizza = CreatePizza(type);
            if (pizza is null)
                throw new ArgumentException("Pizza type not found");

            Console.WriteLine(pizza.Description);
            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();
            return pizza;
        }

        /// <summary>
        /// Фабричный метод, как реализуется в конкретных фабриках.
        /// Отделяет клиентский код от создания объекта,
        /// так как он отвечает за содание объекта(с.157).
        /// </summary>
        /// <param name="type"> тип пиццы. </param>
        /// <returns></returns>
        protected abstract Pizza CreatePizza(string type);
    }
}