﻿namespace AbstractFactory.Factories
{
    using AbstractFactory.Ingredient;
    using AbstractFactory.Pizzas;

    public class ChicagoPizzaStore : PizzaStore
    {

        protected override Pizza CreatePizza(string type)
        {
            var factory = new ChicagoIngredientFactory();
            Pizza pizza;
            switch (type)
            {
                case "cheese":
                    pizza = new CheesePizza(factory);
                    pizza.SetPizzaName("Chicago Style Cheese Pizza");
                    return pizza;
                case "pepperoni":
                    pizza = new PepperoniPizza(factory);
                    pizza.SetPizzaName("Chicago Style Pepperoni Pizza");
                    return pizza;
                case "claim":
                    pizza = new ClaimPizza(factory);
                    pizza.SetPizzaName("Chicago Style Claim Pizza");
                    return pizza;
                case "veggie":
                    pizza = new VeggiePizza(factory);
                    pizza.SetPizzaName("Chicago Style Veggie Pizza");
                    return pizza;
                default:
                    return null;
            }
        }
    }
}