namespace Strategy.Models.QuackModels
{
    using System;

    using Strategy.Interfaces;

    internal class Quack : IQuackBehavior
    {
        public void QuackAction()
        {
            Console.WriteLine("Quack");
        }
    }
}