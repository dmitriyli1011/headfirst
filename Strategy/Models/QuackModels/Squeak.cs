namespace Strategy.Models.QuackModels
{
    using System;

    using Strategy.Interfaces;

    internal class Squeak : IQuackBehavior
    {
        public void QuackAction()
        {
            Console.WriteLine("Squeak");
        }
    }
}