namespace Strategy.Models.QuackModels
{
    using System;

    using Strategy.Interfaces;

    public class MuteQuack : IQuackBehavior
    {
        public void QuackAction()
        {
            Console.WriteLine("<<Silence>>");
        }
    }
}