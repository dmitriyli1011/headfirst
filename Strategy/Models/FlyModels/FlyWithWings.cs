namespace Strategy.Models.FlyModels
{
    using System;

    using Strategy.Interfaces;

    public class FlyWithWings : IFlyBehavior
    {
        public void FlyAction()
        {
            Console.WriteLine("I’m flying!!");
        }
    }
}