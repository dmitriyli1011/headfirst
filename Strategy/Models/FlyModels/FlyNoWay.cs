namespace Strategy.Models.FlyModels
{
    using System;

    using Strategy.Interfaces;

    public class FlyNoWay : IFlyBehavior
    {
        public void FlyAction()
        {
            Console.WriteLine("I cant fly!!");
        }
    }
}