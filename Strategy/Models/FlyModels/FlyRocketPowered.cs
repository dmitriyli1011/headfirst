namespace Strategy.Models.FlyModels
{
    using System;

    using Strategy.Interfaces;

    public class FlyRocketPowered : IFlyBehavior
    {
        public void FlyAction()
        {
            Console.WriteLine("I'm flying with a rocket!!");
        }
    }
}