namespace Strategy.Models
{
    using System;

    using Strategy.Models.Base;
    using Strategy.Models.FlyModels;
    using Strategy.Models.QuackModels;

    public class MallardDuck : Duck
    {
        public MallardDuck()
        {
            _quackBehavior = new Quack();
            _flyBehavior = new FlyWithWings();
        }
        public override void Display()
        {
            Console.WriteLine("I’m a real Mallard duck");
        }
    }
}