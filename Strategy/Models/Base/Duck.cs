namespace Strategy.Models.Base
{
    using System;

    using Strategy.Interfaces;

    /// <summary>
    /// Базовая утка.
    /// </summary>
    public abstract class Duck
    {
        public IFlyBehavior _flyBehavior;
        public IQuackBehavior _quackBehavior;

        public abstract void Display();
        public void PerformFly()
        {
            _flyBehavior.FlyAction();
        }
        public void PerformQuack()
        {
            _quackBehavior.QuackAction();
        }
        public void Swim()
        {
            Console.WriteLine("All ducks float, even decoys!");

        }

        /// <summary>
        /// Динамическое переопределение действия.
        /// Динамическое значит изменение поведения объекта после создания.
        /// </summary>
        public void SetNewQuackAction(IQuackBehavior quack)
        {
            _quackBehavior = quack;
        }

        /// <summary>
        /// Динамическое переопределение действия.
        /// Динамическое значит изменение поведения объекта после создания.
        /// </summary>
        public void SetNewFlyAction(IFlyBehavior fly)
        {
            _flyBehavior = fly;
        }
    }
}