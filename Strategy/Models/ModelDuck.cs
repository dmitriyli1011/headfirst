namespace Strategy.Models
{
    using System;

    using Strategy.Models.Base;
    using Strategy.Models.FlyModels;
    using Strategy.Models.QuackModels;

    public class ModelDuck : Duck
    {
        public ModelDuck()
        {
            _quackBehavior = new Quack();
            _flyBehavior = new FlyNoWay();
        }
        public override void Display()
        {
            Console.WriteLine("I’m a model duck");
        }
    }
}