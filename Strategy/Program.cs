﻿namespace Strategy
{
    using Strategy.Models;
    using Strategy.Models.Base;
    using Strategy.Models.FlyModels;

    /// <summary>
    /// Паттерн поведения.
    /// Стратегия определяет семейство алгоритмов,
    /// инкапсулирует из и обеспечивает взаимозаменяемость.
    /// Позволяет модифицировать алгоритмы вне зависимости от их использования.
    /// (Благодаря реализации одного интерфеса классами алгоритмов.)
    /// </summary>
    class Program
    {
        public static void Main(string[] args)
        {
            Duck mallard = new MallardDuck();
            mallard.PerformFly();
            mallard.PerformQuack();

            Duck modelDuck = new ModelDuck();
            modelDuck.PerformFly();
            modelDuck.SetNewFlyAction(new FlyRocketPowered());
            modelDuck.PerformFly();
        }
    }
}
