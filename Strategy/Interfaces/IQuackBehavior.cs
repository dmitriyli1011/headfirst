namespace Strategy.Interfaces
{
    /// <summary>
    /// Поведение кряканья.
    /// </summary>
    public interface IQuackBehavior
    {
        void QuackAction();
    }
}