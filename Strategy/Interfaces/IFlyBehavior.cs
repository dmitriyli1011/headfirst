namespace Strategy.Interfaces
{
    /// <summary>
    /// Поведение полета.
    /// </summary>
    public interface IFlyBehavior
    {
        void FlyAction();
    }
}