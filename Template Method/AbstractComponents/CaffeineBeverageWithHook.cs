﻿namespace TemplateMethod.AbstractComponents
{
    using System;

    public abstract class CaffeineBeverageWithHook
    {
        public void PrepareRecipe()
        {
            BoilWater();
            Brew();
            PourUp();
            if (NeedCondiments())
            {
                AddCondiments();
            }
        }

        public abstract void AddCondiments();

        public void BoilWater()
        {
            Console.WriteLine("Boiling water");
        }

        public abstract void Brew();

        public void PourUp()
        {
            Console.WriteLine("Pouring into cup");
        }

        public virtual bool NeedCondiments()
        {
            return true;
        }
    }
}