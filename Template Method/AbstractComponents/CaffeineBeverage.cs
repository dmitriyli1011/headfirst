﻿namespace TemplateMethod.AbstractComponents
{
    using System;

    public abstract class CaffeineBeverage
    {
        public abstract void AddComponents();

        public void BoilWater()
        {
            Console.WriteLine("Boiling water");
        }

        public abstract void Brew();

        public void PourUp()
        {
            Console.WriteLine("Pouring into cup");
        }

        /// <summary>
        ///   Шаблонный метод
        /// </summary>
        public void PrepareRecipe()
        {
            BoilWater();
            Brew();
            PourUp();
            AddComponents();
        }
    }
}