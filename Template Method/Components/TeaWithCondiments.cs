﻿namespace TemplateMethod.Components
{
    using System;

    using TemplateMethod.AbstractComponents;

    public class TeaWithCondiments : CaffeineBeverageWithHook
    {
        public override void AddCondiments()
        {
            Console.WriteLine("Adding lemon");
        }

        public override void Brew()
        {
            Console.WriteLine("Steepng tea");
        }

        public override bool NeedCondiments()
        {
            var answer = GetUserInput().ToLower();
            if (answer.StartsWith("y"))
                return true;
            
            return false;
        }

        private string GetUserInput()
        {
            Console.WriteLine("Would you like lemon? (y/n)");
            var answer = string.Empty;
            try
            {
                answer = Console.ReadLine();
                
            }
            catch
            {
                Console.WriteLine("error");
            }
            return answer;
        }
    }
}