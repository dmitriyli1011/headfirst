﻿namespace TemplateMethod.Components
{
    using System;

    public class Tea
    {
        public void AddLeamon()
        {
            Console.WriteLine("Adding lemon");
        }

        public void BoilWater()
        {
            Console.WriteLine("Boiling water");
        }

        public void PourUpCup()
        {
            Console.WriteLine("Pouring into cup");
        }

        public void SteepTeaBag()
        {
            Console.WriteLine("Steeping the tea");
        }

        private void PreapareRecipe()
        {
            BoilWater();
            SteepTeaBag();
            PourUpCup();
            AddLeamon();
        }
    }
}