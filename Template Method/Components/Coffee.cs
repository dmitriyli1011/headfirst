﻿namespace TemplateMethod.Components
{
    using System;

    public class Coffee
    {
        public void AddSugarAndMilk()
        {
            Console.WriteLine("Adding sugar and milk");
        }

        public void BoilWater()
        {
            Console.WriteLine("Boiling water");
        }

        public void BrewCoffeeGrinds()
        {
            Console.WriteLine("Dripping Coffee though filter");
        }

        public void PourUpCup()
        {
            Console.WriteLine("Pouring into cup");
        }

        private void PreapareRecipe()
        {
            BoilWater();
            BrewCoffeeGrinds();
            PourUpCup();
            AddSugarAndMilk();
        }
    }
}