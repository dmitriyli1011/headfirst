﻿namespace TemplateMethod.Components
{
    using System;

    using TemplateMethod.AbstractComponents;

    public class TeaTemp : CaffeineBeverage
    {
        public override void AddComponents()
        {
            Console.WriteLine("Adding lemon");
        }

        public override void Brew()
        {
            //погружать
            Console.WriteLine("Steeping the tea");
        }
    }
}