﻿namespace TemplateMethod.Components
{
    using System;

    using TemplateMethod.AbstractComponents;

    public class CoffeeTemp : CaffeineBeverage
    {
        public override void AddComponents()
        {
            Console.WriteLine("Adding sugar and milk");
        }

        public override void Brew()
        {
            Console.WriteLine("Dripping coffee to the filter");
        }
    }
}