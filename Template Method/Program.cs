﻿using System;

namespace TemplateMethod
{
    using TemplateMethod.Components;

    /// <summary>
    /// Шаблонный Метод определяет основные шаги алгоритма и позволяет
    /// субклассам предоставить реализацию одного или нескольких шагов.
    ///
    /// Паттерн Шаблонный Метод задает «скелет» алгоритма
    /// в методе, оставляя определение реализации некоторых шагов
    /// субклассам.Субклассы могут переопределять некоторые
    /// части алгоритма без изменения его структуры.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            var a = new CoffeeTemp();
            a.PrepareRecipe();

            var tea = new TeaWithCondiments();
            tea.PrepareRecipe();
        }
    }
}
