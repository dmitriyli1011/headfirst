﻿namespace Iterator
{
    using System;

    using Iterator.Collections;
    using Iterator.Iterators;

    using PancakeMenuIterator = Iterator.Collections.PancakeMenuIterator;

    public class Waitress
    {
        /// <summary>
        /// До IMenu было зависимо от конкретной реализации.
        /// Теперь принимается любое меню, реализующее IMenu
        /// </summary>
        private IMenu PancakeMenuIterator;

        private IMenu DinnerMenuIterator;

        public Waitress(IMenu pancakeMenuIterator, IMenu dinnerMenuIterator)
        {
            PancakeMenuIterator = pancakeMenuIterator;
            DinnerMenuIterator = dinnerMenuIterator;
        }

        public void PrintMenu()
        {
            IIterator pancake = PancakeMenuIterator.CreateIterator();
            IIterator dinner = DinnerMenuIterator.CreateIterator();
            PrintMenu(pancake);
            PrintMenu(dinner);
        }

        public void PrintMenu(IIterator iterator)
        {
            while (iterator.HasNext())
            {
                var menuItem = iterator.Next();
                Console.WriteLine("---Start---");
                Console.WriteLine(menuItem.GetName());
                Console.WriteLine(menuItem.GetDescription());
                Console.WriteLine(menuItem.GetPrice());
                Console.WriteLine("---END---");
            }
        }
    }
}