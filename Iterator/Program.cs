﻿using System;

namespace Iterator
{
    using Iterator.Collections;
    using Iterator.Iterators;

    /// <summary>
    /// Идея паттерна Итератор состоит в том,
    /// чтобы вынести поведение обхода коллекции из самой коллекции в отдельный класс.
    ///
    /// Под термином КОЛЛЕКЦИЯ мы подразумеваем группу объектов.
    /// Такие объекты могут храниться в разных структурах
    /// данных: списках, массивах, хеш- картах...но при этом все равно остаются коллекциями.
    ///
    /// Перебор элементов выполняется объектом итератора,
    /// а не самой коллекцией.
    ///
    /// Класс должен иметь только одну причину для изменения с. 370
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            IMenu pancakeMenu = new PancakeMenuIterator();
            IMenu dinnerMenu = new DinnerMenuIterator();

            var waitress = new Waitress(pancakeMenu, dinnerMenu);

            waitress.PrintMenu();
        }
    }
}
