﻿namespace Iterator.Collections
{
    using System.Collections.Generic;

    using Composite.Iterators;

    using Iterator.Items;
    using Iterator.Iterators;

    public class CafeMenuIterator : IMenu
    {
        private Dictionary<string, MenuItem> menuItems = new Dictionary<string, MenuItem>();

        public CafeMenuIterator()
        {
            AddItem("Veggie Burger and Air Fries", 
                "Veggie burger on a whole wheat bun, lettuce, tomato, and fries", 
                true, 
                3.99);

            AddItem("Soup of the day",
                "A cup of soup of the day, with a side salad",
                false,
                3.69);

            AddItem("Burrito",
                "A large burrito, with a whole pinto beans, salsa, guacamole",
                true,
                4.29);
        }

        private void AddItem(string name, string description, bool vegeterian, double price)
        {
            var menuItem = new MenuItem(name, description, vegeterian, price);
            menuItems.Add(menuItem.GetName(), menuItem);
        }

        public IIterator CreateIterator()
        {
            return new CafeIterator(menuItems);
        }
    }
}