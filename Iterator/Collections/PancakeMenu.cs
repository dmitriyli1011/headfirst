﻿namespace Iterator.Collections
{
    using System.Collections.Generic;

    using Iterator.Items;
    using Iterator.Iterators;

    public class PancakeMenu
    {
        private List<MenuItem> menuItems;

        public PancakeMenu()
        {
            menuItems = new List<MenuItem>();

           AddItem("K&B’s Pancake Breakfast",
               "Pancakes with scrambled eggs, and toast",
               true,
               2.99);
           AddItem("Regular Pancake Breakfast",
               "Pancakes with fried eggs, sausage",
               true,
               2.7);
           AddItem("Blueberry Pancakes",
               "Pancakes made with fresh blueberries",
               true,
               3.49);
           AddItem("Waffles",
               "Waffles with your choice of blueberries or strawberries",
               true,
               2.59);
        }

        private void AddItem(string name, string description, bool vegeterian, double price)
        {
            var menuItem = new MenuItem(name, description, vegeterian, price);
            menuItems.Add(menuItem);
        }

        public List<MenuItem> GetMenu()
        {
            return menuItems;
        }
    }

  
}