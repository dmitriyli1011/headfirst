﻿namespace Iterator.Collections
{
    using Iterator.Iterators;

    /// <summary>
    /// Общий интерфейс меню
    /// </summary>
    public interface IMenu
    {
        /// <summary>
        /// Создание итератора для перебора
        /// </summary>
        /// <returns></returns>
        IIterator CreateIterator();
    }
}