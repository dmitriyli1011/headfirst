﻿namespace Iterator.Collections
{
    using System;

    using Iterator.Items;
    using Iterator.Iterators;

    public class DinnerMenuIterator : IMenu
    {
        private const int MAX_SIZE = 6;
        private int NumberOfItems = 0;
        private MenuItem[] menuItems;

        public DinnerMenuIterator()
        {
            menuItems = new MenuItem[MAX_SIZE];

            AddItem("Vegetarian BLT",
                "(Fakin’) Bacon with lettuce & tomato on whole wheat",
                true,
                3.99);
            AddItem("BLT",
                "Bacon with lettuce & tomato on whole wheat",
                false,
                3.7);
            AddItem("Soup of the day",
                "Soup of the day, with a side of potato salad",
                false,
                4.49);
            AddItem("Hotdog",
                "A hot dog, with saurkraut, relish, onions, topped with cheese",
                false,
                3.59);
        }

        private void AddItem(string name, string description, bool vegeterian, double price)
        {
            var menuItem = new MenuItem(name, description, vegeterian, price);
            if (NumberOfItems >= MAX_SIZE)
                Console.WriteLine("Menu is full");
            else
            {
                menuItems[NumberOfItems] = menuItem;
                NumberOfItems++;
            }
        }

        /// <summary>
        /// Возврат конкретного итератора
        /// </summary>
        /// <returns></returns>
        public IIterator CreateIterator()
        {
            return new DinerIterator(menuItems);
        }
    }
}