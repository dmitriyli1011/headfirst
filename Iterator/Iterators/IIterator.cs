﻿namespace Iterator.Iterators
{
    using Iterator.Items;

    public interface IIterator
    {
        public bool HasNext();

        public MenuItem Next();
    }
}