﻿namespace Iterator.Iterators
{
    using System.Collections.Generic;

    using Iterator.Items;

    public class PancakeIterator : IIterator
    {
        private readonly List<MenuItem> Item;

        private int Position = 0;

        public PancakeIterator(List<MenuItem> item)
        {
            Item = item;
        }

        public bool HasNext()
        {
            if (Position >= Item.Count-1 || Item[Position] == null)
                return false;

            return true;
        }

        public MenuItem Next()
        {
            var menuItem = Item[Position];
            Position++;
            return menuItem;
        }
    }
}