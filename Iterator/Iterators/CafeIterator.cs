﻿namespace Composite.Iterators
{
    using System.Collections.Generic;
    using System.Linq;

    using Iterator.Items;
    using Iterator.Iterators;

    public class CafeIterator : IIterator
    {
        private readonly Dictionary<string, MenuItem> Items;

        private int Position;

        public CafeIterator(Dictionary<string, MenuItem> items)
        {
            Items = items;
        }

        public bool HasNext()
        {
            if (Position > Items.Count - 1 || Items.ElementAt(Position).Value == null)
                return false;

            return true;
        }

        public MenuItem Next()
        {
            var menuItem = Items.ElementAt(Position).Value;
            Position++;
            return menuItem;
        }
    }
}