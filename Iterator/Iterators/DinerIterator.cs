﻿namespace Iterator.Iterators
{
    using Iterator.Items;

    public class DinerIterator : IIterator
    {
        private MenuItem[] Item;

        private int Position = 0;

        public DinerIterator(MenuItem[] item)
        {
           Item = item;
        }

        public bool HasNext()
        {
            if (Position >= Item.Length || Item[Position] == null)
            {
                return false;
            }

            return true;
        }

        public MenuItem Next()
        {
            MenuItem menuItem = Item[Position];
            Position++;
            return menuItem;
        }
    }
}