﻿namespace Iterator.Items
{
    public class MenuItem
    {
        private string Name;

        private string Description;

        private bool Vegeterian;

        private double Price;

        public MenuItem(string name, string description, bool vegeterian, double price)
        {
            Name = name;
            Description = description;
            Vegeterian = vegeterian;
            Price = price;
        }

        public string GetName()
        {
            return Name;
        }

        public string GetDescription()
        {
            return Description;
        }

        public bool IsVegeterian()
        {
            return Vegeterian;
        }

        public double GetPrice()
        {
            return Price;
        }
    }
}