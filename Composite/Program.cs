﻿namespace Composite
{
    using Composite.Collections;
    using Composite.Items;

    /// <summary>
    /// Паттерн Компоновщик объединяет объекты в древовидные структуры
    /// для представления иерархий «часть/целое».
    ///
    /// Компоновщикпозво­ляет клиенту выполнять однородные
    /// операции с отдельными объектами и их совокупностями.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            MenuComponent pancakeHouseMenu =
                new MenuComposite("“PANCAKE HOUSE MENU”", "“Breakfast”");
            MenuComponent dinerMenu =
                new MenuComposite("“DINER MENU”", "“Lunch”");
            MenuComponent caffeMenu =
                new MenuComposite("“CAFE MENU”", "“Dinner”");
            MenuComponent dessertMenu =
                new MenuComposite("“DESSERT MENU”", "“Dessert of course!”");

            MenuComponent  allMenu = new MenuComposite("ALL MENU", "All menu combined");
            allMenu.Add(pancakeHouseMenu);
            allMenu.Add(dinerMenu);
            allMenu.Add(caffeMenu);

            dinerMenu.Add(
                new MenuItemComposite(
                    "“Pasta”",
                    "“Spaghetti with Marinara Sauce, and a slice of sourdough bread”",
                    true,
                    3.89));

            pancakeHouseMenu.Add(new MenuItemComposite(
                "K&B's Pancake Breakfast",
                "Pancakes with scrambled eggs and toast",
                true,
                2.99));

            allMenu.Add(dessertMenu);

            dessertMenu.Add(
                new MenuItemComposite(
                    "“Apple Pie”",
                    "“Apple pie with a flakey crust, topped with vanilla icecream”",
                    true,
                    1.59));

            caffeMenu.Add(new MenuItemComposite(
                "Veggie Burger and Air Fries",
                "Veggie burger on a whole wheat bun, lettuce, tomato, and fries",
                true,
                3.99));

            WaitressComposite waitress = new WaitressComposite(allMenu);
            waitress.PrintMenu();
            //waitress.PrintVegetarianMenu();
        }
    }
}
