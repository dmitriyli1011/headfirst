﻿namespace Composite.Iterators
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public class CompositeIterator : IEnumerator<MenuComponent>
    {
        // в нашей ситуации код должен сохранять
        // текущую позицию в комбинационной рекусивной структуре.
        // По этой причине для отслеживания
        // текущей позиции при перемещении вверx/вниз по
        // комбинационной иерархии используются стеки.
        private readonly Stack<IEnumerator<MenuComponent>> stack = new Stack<IEnumerator<MenuComponent>>();

        public CompositeIterator(IEnumerator<MenuComponent> iterator)
        {
            stack.Push(iterator);
        }

        public void Dispose()
        {
        }

        object? IEnumerator.Current => Current;

        public MenuComponent Current
        {
            get
            {
                if (MoveNext())
                {
                    var iterator = stack.Peek();
                    iterator.MoveNext();
                    var component = iterator.Current;
                    stack.Push(component.CreateIterator());
                    return component;
                }

                return null;
            }
        }

        public bool MoveNext()
        {
            if (stack.Count == 0)
                return false;

            var enumerator = stack.Peek();
            if (!enumerator.MoveNext())
            {
                stack.Pop();
                return MoveNext();
            }

            return true;
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }
}