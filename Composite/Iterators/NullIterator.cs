﻿namespace Composite.Iterators
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public class NullIterator : IEnumerator<MenuComponent>
    {
        public bool MoveNext()
        {
            return false;
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }

        public MenuComponent Current => null;

        object? IEnumerator.Current => Current;

        public void Dispose()
        {
        }
    }
}