﻿namespace Composite.Collections
{
    using Composite.Iterators;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Класс комбинационного узла
    /// </summary>
    public class MenuComposite : MenuComponent
    { 
        IEnumerator<MenuComponent> iterator;

        private List<MenuComponent> menuComponents
            = new List<MenuComponent>();

        private string Name;

        private string Description;

        public MenuComposite(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public override void Add(MenuComponent menuComponent)
        {
            menuComponents.Add(menuComponent);
        }

        public override void Remove(MenuComponent menuComponent)
        {
            menuComponents.Remove(menuComponent);
        }

        public override MenuComponent GetChild(int position)
        {
            return menuComponents[position];
        }

        public override string GetName()
        {
            return Name;
        }

        public override string GetDescription()
        {
            return Description;
        }

        public override void Print()
        {
            Console.WriteLine(" " + GetName());
            Console.WriteLine("--" + GetDescription());
            Console.WriteLine("-----------");

            // ВНИМАНИЕ: если в процессе перебора мы встретим другой
            // объект меню, его метод print() начнет новый перебор, и т. д
            using (var enumerator = menuComponents.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    MenuComponent menuComponent = enumerator.Current;
                    menuComponent.Print();
                }
            }
            
        }

        public override IEnumerator<MenuComponent> CreateIterator()
        {
            var a = menuComponents.GetEnumerator();
            return iterator ??= new CompositeIterator(menuComponents.GetEnumerator());
        }
    }
}