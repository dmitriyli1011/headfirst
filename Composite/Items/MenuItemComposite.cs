﻿namespace Composite.Items
{
    using Composite.Iterators;
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///элемент меню MenuItem
    /// 
    /// Это класс листового узла на диаграмме классов
    /// паттерна Компоновщик, и он реализует поведение
    /// элементов комбинации
    /// </summary>
    public class MenuItemComposite : MenuComponent
    {
        private string Name;

        private string Description;

        private bool Vegeterian;

        private double Price;

        public MenuItemComposite(string name, string description, bool vegeterian, double price)
        {
            Name = name;
            Description = description;
            Vegeterian = vegeterian;
            Price = price;
        }

        public override string GetName()
        {
            return Name;
        }

        public override string GetDescription()
        {
            return Description;
        }

        public override double GetPrice()
        {
            return Price;
        }

        public override bool IsVegetarian()
        {
            return Vegeterian;
        }

        public override void Print()
        {
            Console.WriteLine(" " + GetName());
            if (IsVegetarian() == true)
                Console.WriteLine("(v)");

            Console.WriteLine(" " + GetPrice());
            Console.WriteLine("--" + GetDescription());
        }

        public override IEnumerator<MenuComponent> CreateIterator()
        {
            return new NullIterator();
        }
    }
}