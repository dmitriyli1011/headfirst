﻿namespace Composite
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Design;
    using Composite.Iterators;

    public abstract class MenuComponent
    {
        public virtual void Add(MenuComponent menuComponent)
        {
        }

        public virtual void Remove(MenuComponent menuComponent)
        {
        }

        public virtual MenuComponent GetChild(int position)
        {
            throw new NotImplementedException();
        }

        public virtual string GetName()
        {
            throw new NotImplementedException();
        }

        public virtual string GetDescription()
        {
            throw new NotImplementedException();
        }

        public virtual double GetPrice()
        {
            throw new NotImplementedException();
        }

        public virtual bool IsVegetarian()
        {
            throw new NotImplementedException();
        }

        public virtual void Print()
        {
        }

        public virtual IEnumerator<MenuComponent> CreateIterator()
        {
            throw new NotImplementedException();
        }
    }
}