﻿namespace Composite
{
    using System;

    public class WaitressComposite
    {
        private MenuComponent AllMenu;

        public WaitressComposite(MenuComponent allMenu)
        {
            AllMenu = allMenu;
        }

        public void PrintMenu()
        {
            AllMenu.Print();
        }

        //TODO Различия с Java Iterator
        // .NET-е IEnumerator отличается от Java-го Iterator следующим:
        //
        // Iterator после построения указывает на первый элемент коллекции
        // (или, для пустой коллекции, является недопустимым, и hasNext немедленно вернет false ),
        //  IEnumerator указывает изначально перед первым элементом коллекции
        //  (для пустой коллекции MoveNext вернет false )
        //
        // Iterator имеет метод hasNext, в то время как для IEnumerator вы проверяете результат
        //  метода MoveNext
        //
        // Iterator имеет метод next, в то время как для IEnumerator вы также используете MoveNext
        //
        // Iterator 's next возвращает следующий элемент, в то время как с IEnumerator вы
        //  используете свойство Current после вызова MoveNext
        //
        // Iterator в Java имеет метод remove, который позволяет удалять элементы из базовой коллекции.
        // В IEnumerator нет эквивалента
        public void PrintVegetarianMenu()
        {
            var iterator = AllMenu.CreateIterator();
            Console.WriteLine("-----Vegetarian menu-----");

            while (iterator.MoveNext())
            {
                var component = iterator.Current;
                try
                {
                    if (component?.IsVegetarian() == true)
                        component.Print();
                }
                catch (NotImplementedException)
                {
                    Console.WriteLine("__e__");
                }
            }
            
        }
    }
}