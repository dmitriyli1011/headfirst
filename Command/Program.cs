﻿using System;

namespace Command
{
    using Command.Commands;
    using Command.Objects;
    using Command.RemoteControls;

    class Program
    {
        /// <summary>
        /// Паттерн поведения.
        /// Создание запроса в виде объекта.
        /// Функции обратного действия, вызываемые в ответ на другие действия.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            SimpleRemoteControl remoteControl = new SimpleRemoteControl();
            Light light = new Light("My Room");
            LightOnCommand lightOn = new LightOnCommand(light);

            GarageDoor garageDoor = new GarageDoor();
            GarageDoorOpenCommand doorOpenCommand = new GarageDoorOpenCommand(garageDoor);

            remoteControl.SetCommand(lightOn);
           // remoteControl.ButtonWasPressed();
           // Console.WriteLine();
           // remoteControl.SetCommand(doorOpenCommand);
          //  remoteControl.ButtonWasPressed();

            //**************************
            RemoteControl control = new RemoteControl();
            Light roomLight = new Light("Living Room Light");
            Stereo stereo = new Stereo();
            GarageDoor garageDoorSecond = new GarageDoor();

            LightOnCommand lightOnCommand = new LightOnCommand(roomLight);
            LightOffCommand lightOffCommand = new LightOffCommand(roomLight);

            StereoOnWithCDCommand stereoOnWithCdCommand = new StereoOnWithCDCommand(stereo);
            StereoOffWithCDCommand stereoOffWithCdCommand = new StereoOffWithCDCommand(stereo);

            GarageDoorOpenCommand garageDoorOpenCommand = new GarageDoorOpenCommand(garageDoorSecond);
            GarageDoorCloseCommand garageDoorCloseCommand= new GarageDoorCloseCommand(garageDoorSecond);

            control.SetCommand(0, lightOnCommand, lightOffCommand);
            control.SetCommand(1, stereoOnWithCdCommand, stereoOffWithCdCommand);
            control.SetCommand(2, garageDoorOpenCommand, garageDoorCloseCommand);

            //(с. 242)
            //Console.WriteLine(control);

            //control.OnButtonWasPushed(0);
            //control.OnButtonWasPushed(1);
            //control.OnButtonWasPushed(2);
            //control.OffButtonWasPushed(0);
            //control.OffButtonWasPushed(1);
            //control.OffButtonWasPushed(2);

            //*********************
            Console.WriteLine();
            RemoteControl control2 = new RemoteControl();
            control2.SetCommand(0, lightOnCommand, lightOffCommand);
            control2.OnButtonWasPushed(0);
            control2.OffButtonWasPushed(0);
            Console.WriteLine(control2);
            control2.UndoButtonWasPushed();
            control2.OffButtonWasPushed(0);
            control2.OnButtonWasPushed(0);
            Console.WriteLine(control2);
            control2.UndoButtonWasPushed();
        }
    }
}
