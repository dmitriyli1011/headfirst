﻿namespace Command.Objects
{
    using System;

    public class Stereo
    {
        public void On()
        {
            Console.WriteLine("Stereo is On");
        }
        public void Off()
        {
            Console.WriteLine("Stereo is Off");
        }
        public void SetCd()
        {
            Console.WriteLine("Set CD");
        }
        public void SetVolume(int vol)
        {
            Console.WriteLine("Set Volume" + vol);
        }
    }
}