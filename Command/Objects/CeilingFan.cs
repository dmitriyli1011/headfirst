﻿namespace Command.Objects
{
    public class CeilingFan
    {
        public static int HIGH = 3;
        public static int MEDIUM = 2;
        public static int LOW = 1;
        public static int OFF = 0;
        string location;
        int speed;

        public CeilingFan(string location)
        {
            this.location = location;
            speed = OFF;
        }
        public void High()
        {
            speed = HIGH;
            // Высокая скорость
        }
        public void Medium()
        {
            speed = MEDIUM;
            // Средняя скорость
        }
        public void Low()
        {
            speed = LOW;
            // Низкая скорость
        }

        public void Off()
        {
            speed = OFF;
            // Выключение вентилятора
        }
        public int GetSpeed()
        {
            return speed;
        }
    }
}