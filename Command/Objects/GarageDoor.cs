﻿namespace Command.Objects
{
    using System;

    public class GarageDoor
    {
        public void Up()
        {
            Console.WriteLine("Garage Door is Open");
        }

        public void Down()
        {
            Console.WriteLine("Garage Door is Close");
        }
        public void Stop()
        {
            Console.WriteLine("Garage Door Stops");
        }
        public void LightOn()
        {
            Console.WriteLine("Light On in Garage");
        }
        public void LightOff()
        {
            Console.WriteLine("Light Off in Garage");
        }
    }
}