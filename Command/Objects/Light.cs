﻿namespace Command.Objects
{
    using System;

    /// <summary>
    /// Объект, которым будет управлять команда.
    /// </summary>
    public class Light
    {
        private string Name;

        public Light(string name)
        {
            Name = name;
        }
        public void Off()
        {
            Console.WriteLine("Light is Off " + Name);
        }
        public void On()
        {
            Console.WriteLine("Light in On " + Name);
        }
    }
}