﻿namespace Command.RemoteControls
{
    using System.Text;

    using Command.Commands;

    public class RemoteControl
    {
        private ICommand[] _onCommands;
        private ICommand[] _offCommands;
        private ICommand _command;

        public RemoteControl()
        {
            _onCommands = new ICommand[7];
            _offCommands = new ICommand[7];

            for (var i = 0; i < 7; i++)
            {
                _onCommands[i] = new NoCommand();
                _offCommands[i] = new NoCommand();
            }
        }

        public void SetCommand(int slot, ICommand onCommand, ICommand offCommand)
        {
            _onCommands[slot] = onCommand;
            _offCommands[slot] = offCommand;
        }

        public void OnButtonWasPushed(int slot)
        {
            _onCommands[slot].Execute();
            _command = _onCommands[slot];
        }

        public void OffButtonWasPushed(int slot)
        {
            _offCommands[slot].Execute();
            _command = _offCommands[slot];
        }

        public void UndoButtonWasPushed()
        {
            _command.Undo();
        }

        public override string ToString()
        {
            var stringBuffer = new StringBuilder();
            stringBuffer.Append("\n----- Remote Control -----\n");
            for (var i = 0; i < _onCommands.Length; i++)
            {
                stringBuffer.Append("[slot " + i + "]" + _onCommands[i].GetType().Name + 
                                    "    " + _offCommands[i].GetType().Name + "\n");
            }

            return stringBuffer.ToString();
        }
    }
}