﻿namespace Command.RemoteControls
{
    using Command.Commands;

    /// <summary>
    /// Инициатор
    /// </summary>
    public class SimpleRemoteControl
    {
        private ICommand slot;

        public SimpleRemoteControl()
        {
        }

        /// <summary>
        /// Передача конкретной команды
        /// </summary>
        /// <param name="command"></param>
        public void SetCommand(ICommand command)
        {
            slot = command;
        }

        /// <summary>
        /// Отправляется запрос.
        /// </summary>
        public void ButtonWasPressed()
        {
            slot.Execute();
        }

    }
}