﻿namespace Command.Commands
{
    using Command.Objects;

    public class GarageDoorOpenCommand : ICommand
    {
        private GarageDoor _garageDoor;
        public GarageDoorOpenCommand(GarageDoor door)
        {
            _garageDoor = door;
        }

        public void Execute()
        {
            _garageDoor.Up();
        }

        public void Undo()
        {
            _garageDoor.Down();
        }
    }

    public class GarageDoorCloseCommand : ICommand
    {
        private GarageDoor _garageDoor;
        public GarageDoorCloseCommand(GarageDoor door)
        {
            _garageDoor = door;
        }

        public void Execute()
        {
            _garageDoor.Down();
        }

        public void Undo()
        {
            _garageDoor.Up();
        }
    }
}