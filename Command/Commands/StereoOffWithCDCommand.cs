﻿namespace Command.Commands
{
    using Command.Objects;

    public class StereoOffWithCDCommand : ICommand
    {
        private Stereo _stereo;

        public StereoOffWithCDCommand(Stereo stereo)
        {
            _stereo = stereo;
        }

        public void Execute()
        {
            _stereo.Off();
        }

        public void Undo()
        {
           _stereo.On();
        }
    }
}