﻿namespace Command.Commands
{
    /// <summary>
    /// Объект NoCommand является примером пустого (null) объекта. Пустые объекты
    /// применяются тогда, когда вернуть «полноценный» объект невозможно, но вам хо-
    /// чется избавить клиента от необходимости проверять null-ссылки.
    /// </summary>
    public class NoCommand : ICommand
    {
        public void Execute()
        {
        }

        public void Undo()
        {
        }
    }
}