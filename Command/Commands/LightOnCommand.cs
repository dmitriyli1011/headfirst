﻿namespace Command.Commands
{
    using Command.Objects;

    /// <summary>
    /// Команда включения света.
    /// </summary>
    public class LightOnCommand : ICommand
    {
        private Light _light;

        public LightOnCommand(Light light)
        {
            _light = light;
        }

        public void Execute()
        {
            _light.On();
        }

        public void Undo()
        {
            _light.Off();
        }
    }
}